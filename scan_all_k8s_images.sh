#/bin/bash

if [ ! -f /usr/local/bin/trivy ]; then
    echo "Trivy not found! Please install it from https://github.com/aquasecurity/trivy"
fi

for image in `kubectl get pods --all-namespaces -o jsonpath="{..image}" |\
tr -s '[[:space:]]' '\n' |\
sort |\
uniq -c | awk '{print $2}'`; do trivy image  -s HIGH,CRITICAL $image; done